import { useState } from "react";
import InputExpensive from "./components/inputExpensive/InputExpensive";
import ExpensiveItems from "./components/ExpensiveItems/ExpensiveItems";
import Filter from "./components/filter/ExpensiveFilter";

const App = () => {

  // state of all the expensives 
  const [expensives, setExpensives] = useState([])
  // state of the expensives that is displayed
  const [displayExpensives, setDisplay] = useState(expensives)

  /* 
    auxiliar function to merge new value
    to existing expensives array
  */
  const addExpensive = newExpensiveItem => {
    setExpensives([...expensives, newExpensiveItem])
    setDisplay([...displayExpensives, newExpensiveItem])
  }

  const changeDisplay = () => {
    let setYear = 2022
    let newDisplay = expensives.map((item) => {
      let expensiveYear = Number(new Date(item.date).getUTCFullYear());
      if (expensiveYear === setYear) {
        return item
      }
    })
    setDisplay([...newDisplay])
  }

  return (
    <div className='main-app'>
      <InputExpensive onNewExpensive={addExpensive} />
      <Filter />
      <ExpensiveItems items={displayExpensives} />
      <button onClick={changeDisplay}>
        click
      </button>
    </div>
  );
}

export default App;
