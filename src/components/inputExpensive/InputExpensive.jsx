import { Form, Button, Card } from "react-bootstrap"
import './inputExpensive.css'

const InputExpensive = ({ onNewExpensive }) => {
    let item = {};

    // auxiliar function to clean alll inputs
    const cleanForm = () => {
        document.getElementById("name").value = ''
        document.getElementById("price").value = ''
        document.getElementById("date").value = ''
    }

    // auxiliar funtion to add values from inputs
    const addExpensiveItem = event => {
        item[event.target.id] = event.target.value
    }

    // update the state of the expensives with the new values inserted
    const submit = () => {
        item.date = new Date(item.date)
        onNewExpensive(item)
        cleanForm()
    }
    
    return (
        <Card className="form-content">
            <div className="md-3">
                <Form.Label htmlFor="name">Item</Form.Label>
                <Form.Control onChange={addExpensiveItem} type="text" id="name" />
            </div>
            <div className="md-3">
                <Form.Label htmlFor="price">Price</Form.Label>
                <Form.Control onChange={addExpensiveItem} type="number" id="price" />
            </div>
            <div className="md-3">
                <Form.Label htmlFor="date">Date</Form.Label>
                <Form.Control onChange={addExpensiveItem} type="date" id="date" />

            </div>
            <div className='d-grid gap-2'>
                <Button onClick={submit} type="submit" variant="outline-primary">
                    Submit
                </Button>
            </div>
        </Card>
    )
}

export default InputExpensive