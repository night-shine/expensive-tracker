import React from "react";

import './expensiveItem.css'
import { Button, Card } from "react-bootstrap";

const Item = ({ item }) => {

    // return number formated in USD currency, return string
    const priceFormat = number => {
        return (new Intl.NumberFormat('en', { style: 'currency', currency: 'USD' }).format(number))
    }

    // format the date to implement on jsx, returns JSX
    const dateFormat = data => {
        let date = new Date(data)
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let day = date.getUTCDate();
        let month = months[date.getUTCMonth()]
        let year = date.getUTCFullYear()
        return (
            <>
                <Card.Title>{day}</Card.Title>
                <Card.Title>{month}</Card.Title>
                <Card.Title>{year}</Card.Title>
            </>
        )
    }

    return (
        <Card>
            <Card.Body className="expensive-card">
                <Card className="date-box">{dateFormat(item.date)}</Card>
                <div className="box-item">
                    <Card.Title>{item.name}</Card.Title>
                </div>
                <div className="box-item">
                    <Button type='a' variant='primary'>
                        {priceFormat(item.price)}
                    </Button>
                </div>
            </Card.Body>
        </Card>
    )
}
export default Item