import React from "react";
import Item from "./Items";


const ExpensiveItems = ({ items }) => {
    return (
        <div>
            <hr />
            {items.map((data) => {
                return <Item item={data} />
            })}
        </div>

    )
}
export default ExpensiveItems
